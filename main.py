import requests, json
token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjMwWi1QWk15ZURuZDFlTHdsa2Z2NEVBRWhtayIsImtpZCI6IjMwWi1QWk15ZURuZDFlTHdsa2Z2NEVBRWhtayJ9.eyJpc3MiOiJodHRwczovL2lkZW50aXR5LmZoaWN0Lm5sIiwiYXVkIjoiaHR0cHM6Ly9pZGVudGl0eS5maGljdC5ubC9yZXNvdXJjZXMiLCJleHAiOjE2MTgyNTYwNTQsIm5iZiI6MTYxODI0ODg1NCwiY2xpZW50X2lkIjoiYXBpLWNsaWVudCIsInVybjpubC5maGljdDp0cnVzdGVkX2NsaWVudCI6InRydWUiLCJzY29wZSI6WyJvcGVuaWQiLCJwcm9maWxlIiwiZW1haWwiLCJmaGljdCIsImZoaWN0X3BlcnNvbmFsIiwiZmhpY3RfbG9jYXRpb24iXSwic3ViIjoiZTlmOTRmMmItZWNkZS00Yzc5LWI3ZDktZDI0ODQ5NmIwODdmIiwiYXV0aF90aW1lIjoxNjE4MjQ4ODU0LCJpZHAiOiJmaGljdC1zc28iLCJyb2xlIjpbInVzZXIiLCJzdHVkZW50Il0sInVwbiI6Ikk0MTk1MjZAZmhpY3QubmwiLCJuYW1lIjoiTG9vbixLb29zIEouVC4gdmFuIiwiZW1haWwiOiJrb29zLnZhbmxvb25Ac3R1ZGVudC5mb250eXMubmwiLCJ1cm46bmwuZmhpY3Q6c2NoZWR1bGUiOiJjbGFzc3xJMi1EQjAxIiwiZm9udHlzX3VwbiI6IjQxOTUyNkBzdHVkZW50LmZvbnR5cy5ubCIsImFtciI6WyJleHRlcm5hbCJdfQ.XltvZol0Ht3O5UYzu5rOlkcsYa52W5T-bfvaxqJk5uNn2iyqIv8iqOJJriwTPIiuz6STmuikohjwnyOxlC0Hvg9fck2BZxmMBo-DSaEljT995CzeUtmS5sVNYT-Zm_EQDX_1os6JK6yPU1_eh37-yxYdCnVv6fdW6BpterT2j9MdGjJcdMvZf_W4hcSII2kWkuP2qI1sXUT0OOhhtQn2McAz_eiT2Y1F8Q8unSKGNR4DXjiu0D1_0lIMnWX1GP92GvNV5lEkq96ghAbjIS_T-RFRYxmsDeTBlixhGSqpFSuWQn3b39zQx-LE7R87d63AOA3g6zW621Fqx6BvJrg3cQ"

def api_request(url):
    response = requests.get(url, headers={"Authorization":"Bearer " + token})
    return response

def iterate_multidimensional(my_dict):
    for k,v in my_dict.items():
        if(isinstance(v,dict)):
            print(k+":")
            iterate_multidimensional(v)
            continue
        print(k+" : "+str(v))


#response_API = requests.get('https://api.fhict.nl/buildings', headers={"Authorization":"Bearer " + token})
#print(response_API.status_code)
#eindhoven = response_API.json.
# for i in response_API.json():
#     print(i)
#     data = i
#     if data['id']=='R1':
#         #print('R1')
#         eindhoven = i
#     if data['id']=='P1':
#         #print('P1')
#         tilburg = i
#iterate_multidimensional(eindhoven)
#iterate_multidimensional(tilburg)

for i in api_request('https://api.fhict.nl/buildings').json():
    if i["id"] in ('R1', 'P1'):
        iterate_multidimensional(i)


#response_API = requests.get('https://api.fhict.nl/rooms/occupancy/default')
#print(response_API)
print(api_request('https://api.fhict.nl/rooms/occupancy/default').json())
